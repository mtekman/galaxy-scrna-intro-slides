# Settings
LECTURE_DIR=lectures
HTML_LECTURE_DIR=$(LECTURE_DIR)/_html
PDF_LECTURE_DIR=$(LECTURE_DIR)/_pdf
TEMPLATE_LECTURE_DIR=$(LECTURE_DIR)/templates
EXERCISE_DIR=exercises
PDF_EXERCISE_DIR=$(EXERCISE_DIR)/_pdf
TEMPLATE_EXERCISE=$(EXERCISE_DIR)/templates/template.latex

LECTURE_SOURCES := $(wildcard $(LECTURE_DIR)/*.md)
HTML_OUTPUTS := $(subst .md,.html,$(subst $(LECTURE_DIR),$(HTML_LECTURE_DIR),$(LECTURE_SOURCES)))
PDF_OUTPUTS := $(subst .md,.pdf,$(subst $(LECTURE_DIR),$(PDF_LECTURE_DIR),$(LECTURE_SOURCES)))

EXERCISE_SOURCES := $(wildcard $(EXERCISE_DIR)/*.tex)
PDF_EXERCISE_OUTPUTS := $(subst .tex,.pdf,$(subst $(EXERCISE_DIR),$(PDF_EXERCISE_DIR),$(EXERCISE_SOURCES)))


default: help

node_modules/.bin/markdown-to-slides:
	npm install markdown-to-slides

node_modules/.bin/decktape:
	npm install decktape

html_lecture: $(HTML_OUTPUTS) lectures/templates/style.css lectures/templates/template.html ## generate HTML slides for one lecture: need to provide lecture name with lecture=lecture_name
pdf_lecture: $(PDF_OUTPUTS) ## generate PDF of the HTML slides for one lecture: need to provide lecture name with lecture=lecture_name
.PHONY: pdf_lecture html_lecture

# Make HTML dir if not existing
$(HTML_LECTURE_DIR):
	mkdir -p $(HTML_LECTURE_DIR)

# Build individual lectures as HTML
$(HTML_LECTURE_DIR)/%.html: $(LECTURE_DIR)/%.md node_modules/.bin/markdown-to-slides $(HTML_LECTURE_DIR)
	node_modules/.bin/markdown-to-slides \
		$< \
		-o $@ \
		--template $(TEMPLATE_LECTURE_DIR)/template.html \
		--style $(TEMPLATE_LECTURE_DIR)/style.css \
		--include-remark

# Make the lecture dir if not existing
$(PDF_LECTURE_DIR):
	mkdir -p $(PDF_LECTURE_DIR)

#Build individual lectures.
$(PDF_LECTURE_DIR)/%.pdf: $(HTML_LECTURE_DIR)/%.html node_modules/.bin/decktape $(PDF_LECTURE_DIR)
	node_modules/.bin/decktape \
		file://`pwd`/$< \
		--no-sandbox \
		--chrome-arg=--allow-file-access-from-files \
		$@


# Make the exercise dir if not eixsting
$(PDF_EXERCISE_DIR):
	mkdir -p $(PDF_EXERCISE_DIR)

pdf_exercise: $(PDF_EXERCISE_OUTPUTS) ## generate PDF of one exercise sheet: need to provide lecture name with exercise=exercise_name

# Build a single pdf from a single tex file
$(PDF_EXERCISE_DIR)/%.pdf: $(EXERCISE_DIR)/%.tex $(PDF_EXERCISE_DIR)
	cd $(PDF_EXERCISE_DIR) && \
		TEXINPUTS="../../:../:" pdflatex \
			../../$(subst .pdf,,$<)

	rm -f $(PDF_EXERCISE_DIR)/*.aux
	rm -f $(PDF_EXERCISE_DIR)/*.log
	rm -f $(PDF_EXERCISE_DIR)/*.out

clean_exercises: ## Clean exercises
	rm -rf $(PDF_EXERCISE_DIR)
.PHONY: clean_exercises

clean_lectures: ## Clean lectures
	rm -rf $(PDF_LECTURE_DIR) $(HTML_LECTURE_DIR)
.PHONY: clean_lectures

clean: clean_exercises clean_lectures ## Clean all
.PHONY: clean

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help
