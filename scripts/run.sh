#!/bin/bash

## Find this info at
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""

output_video=$1
html_page=$2
orgmode_file=$3

if [ "$output_video" = "" ]; then
    echo "
`basename $0` <output-video-with-ext> <input-html> <input-orgmodefile>

Take an input HTML file of slides, an org-mode script file, an image prefix, and generate a video from it."
    exit -1
fi

script_file=$(mktemp)
image_prefix=$(mktemp -d)/images
echo "script-file: $script_file"
echo "image-pref: $image_prefix"

echo mkdir -p $(dirname $output_video) &&
    ./slides_to_images.sh ${html_page} ${image_prefix} &&
    ./orgnotes_to_wordscript.sh ${orgmode_file} ${script_file} &&
    ./generate_video.sh ${output_video} ${script_file} ${image_prefix}