#!/bin/bash

orgfile=$1
outscript=$2

if [ "$orgfile" = "" ]; then
    echo "`basename $0` <org-file> <out-script>

Convert an org-mode document into a single script file for voice processing. Not all text is exported, only the lines which directly follows a second level headline, e.g.:

* First-level Header
** Second-level Header

Text that will be exported first.
This text follows on but wont be exported.

* Another first-level Header
** Another second-level Header

Text that will be exported second.
This text follows on but wont be exported.
"
    exit -1
fi

grep -A 1 "^\*\* " $orgfile\
    | grep -v "^\*\*"\
    | grep -v "^--$"\
    | sed -r 's|^\s+||g' > $outscript