#!/bin/bash

vid_file=$1
script_file=$2
image_prefix=$3

if [ "$vid_file" = "" ]; then
    echo "`basename $0` <output video> <input script> <[images]>

Generates a video using AWS Polly to synethesis text from the input script and combine them with the images.

Environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must be set too.
"
    exit -1
fi

if [ "$AWS_ACCESS_KEY_ID" = "" ]; then
    echo "No AWS_ACCESS_KEY_ID found."
    exit -1
fi

mkdir -p $(dirname $vid_file) &&
# run_ari.R  <output> <script> <accesskey> <secretkey> <images>.
# ./scripts/run_ari_spin.R test.mp4 test.script key key <images> 2>&1

# This also checks that the number of images == script length
./run_ari_spin.R  ${vid_file} ${script_file}\
                  $AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY\
                  ${image_prefix}*.png 2>&1