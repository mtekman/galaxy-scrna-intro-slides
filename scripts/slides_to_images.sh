#!/bin/bash

html_page=$1
image_prefix=$2

if [ "$html_page" = "" ]; then
    echo "`basename $0` <html_slides> <out_image_prefix>

This converts all slides from an html page to images in an output directory of images given by the prefix."
    exit -1
fi

out_dir=$(dirname $image_prefix)
temp_pdf=${image_prefix}.pdf
echo "image-pdf: $temp_pdf"

if [ "$out_dir" = "" ]; then
    echo "image prefix should not be the same directory as current"
    exit -1
fi

mkdir -p $out_dir &&

    wkhtmltopdf --enable-local-file-access\
            --page-height 150\
            --page-width 200\
            $html_page $temp_pdf &&

    pdftopng -r 300 $temp_pdf $image_prefix