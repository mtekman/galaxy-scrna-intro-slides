# Galaxy Lecture Slides and Video

This generates HTML slides based on a markdown template, and then using a seperate org-mode script file of text to be spoken, generates a video of it with actual synthesized speech.

## Requirements

Install LaTeX and NodeJS, and create an AWS account with Polly permissions for the speech synethsize stuff.

If you have not set up Amazon polly before, follow the instructions [here](https://docs.aws.amazon.com/polly/latest/dg/setting-up.html) and ensure that you have the following *Permission policies* applied:

* AmazonS3FullAccess
* AmazonS3ReadOnlyAccess
* AmazonPollyReadOnlyAccess
* AmazonPollyFullAccess

### Slide decks for lectures

  The scripts and template for the markdown to HTML is taken from the BackofenLab's internal templating repo, which is also based on the [GTN Training Material](https://github.com/galaxyproject/training-material) repo.

- Generate the HTML slides for the lecture (will be stored in `lectures/_html`)

    ```
    $ make html_lecture
    ```

- Generate PDF of the HTML slides for the lecture (will be stored in `lectures/_pdf`)

    ```
    $ make pdf_lecture
    ```

### Video

This takes an HTML file of slides and a script of text and generates a video from it.

The video generating R script `run_ari_spin.R` was taken from the Delphine-L's [video-lectures](https://github.com/galaxyproject/video-lectures/) (which was forked off James Taylor's [covid-lessons](https://github.com/jxtx/covid-lessons).


The other scripts are just stuff I cobbled together to take an arbitrary HTML slides file (i.e. the one generated from the source files in this repo), and an org-mode file documenting all text in each slide.

To generate the current video, ensure you have obtained the required AWS credentials (access ID and secret key), then place them in `scripts/run.sh` script. After that, run: 

```bash
$ cd scripts;
$ ./run.sh output/video.mp4 ../lectures/_html/14_single_cell.html ../lectures/14_single_cell.script.org
```

#### Acknowledgements

Cheers to Berenice and Björn for pointing me to the video-lectures.