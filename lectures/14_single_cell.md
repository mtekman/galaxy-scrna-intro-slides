name: title_slide
class: center, middle
count: false

# Single-cell RNA-seq

An introduction to scRNA-seq data analysis

#### Mehmet Tekman and Wendi Bacon


---


### Bulk RNA-Seq

.pull-left-large[![:scale 105%](../img/singlecell/rna_cells_bulkrez.svg)]

.pull-right[
.center[*Summary*]

* Resolution:
  * Entire tissues

* Signal:
  * Average gene expression per tissue

* Differential Expression:
  * Difference between average gene expression between tissues
  * Types of DE normalisation techniques to use?
]

???
* Mention DESeq and some others from previous lecture

---

### Single Cell RNA-Seq

.pull-left-large[![:scale 130%](../img/singlecell/rna_cells_singlerez.svg)]

.pull-right[
.center[*Summary*]

* Resolution:
  * Individual cells within tissues

* Signal:
  * Individual gene expression per cell

* Differential Expression:
  * Some cells express the same set of genes in the same way
  * Difference between one set of cells against another

]


???
* What are we comparing?
  * Difference between cells?
  * Difference between groups of cells?



---

### From Bulk RNA to Single Cell RNA


* In order to quantify RNA at the level of individual cells:

  * New methods of library preparation
  
  * New methods of sequencing
  
  * New methods of quality control
  
  * New methods of analysis

---

### Cell Capture and Replicates

.center[*How do we prepare samples for sequencing?*]

--


.pull-left[

__Bulk RNA-seq__

  1. Cut a thin slice of a tissue
  1. Add enzyme to break down cell walls
  1. Rinse out the unwanted DNA / RNA material
  1. Perform sequencing on leftover goop

]

--

.pull-left[

__Single-cell RNA-seq__

  1. Cut a thin slice of a tissue
  1. Breakdown a tissue into cells
  1. Isolate each cell
     * Add enzyme to break down cell walls
     * Perform barcoding
  1. Perform sequencing in a common pool

]

--

__Biological Replicates__

* Bulk RNA-seq: 
  * Each tissue slice is a sample, can take another slice
* Single-cell RNA-seq:
  * Each cell is sample, cannot directly replicate because unique



???
Small note about replicates:
 * Each sample in single cell is a cell. So you can't take another slice of tissue, break down the cells and call these new cells replicates
 * Can't directly replicate a cell because cells are unique 


---

### Capture / Sorting:

.pull-left[
*How are cells isolated?*
]

--

.pull-right[![:scale 100%](../img/singlecell/mouthpipette.jpg)]

.pull-left[
* Manual pipette:
   * Use a thin glass tube to suction up a cell
   * Maintain pressure in tube
   * Transport to new environment
   * Release pressure in tube
]

--

.pull-left[
* Repeat 1000 times to isolate 1000 cells
  * Error-prone
]

???
Maintain your sanity...

--

.pull-left[

* Automatic pipette:
  * Flow cytometry
]


---

### Capture / Sorting: Flow Cytometry

.pull-right[![:scale 100%](../img/singlecell/opticssystem.png)]

.pull-left-large[
* Stream cells along a liquid through a narrow tube
  * Narrow to permit one cell at a time
  * Fluid enough to allow high-throughput.
]

.pull-left-large[
* Screen each cell with a laser to probe properties:
   * Cell Size and Type
      * Front scatter vs Side scatter
   * Cell Type by Fluorescent Labelling
     *  Cell Surface Markers (CDs)
     *  Fluorescent Labelling

]

.pull-left-large[
* Isolate a cell into its own sequencing environment
]

???
Cell quality by density of cell
Properties of cells by probing up to 30 different fluorescent detectors at a time.
Cell surface markers are little fluorescent antibodies on the surface of the cell that light up at particular light frequencies, and can tell you what type of cell you are looking at. Often multiple light up and you have to guess the cell by the ratio of one cell type to another

* Cell Size and Type
   * Forward scatter vs Side scatter

* Cell Type by Fluorescent Labelling
     *  Cell Surface Markers (CDs)
     *  Fluorescent Labelling


Isolate cell into its own sequencing environment
* Ensure no two cells are in the same environment by accident.

---

### Capture / Sorting: Size and Type

.pull-right[
  ![:scale 100%](../img/singlecell/opticssystem.png)
]


.pull-left[
*Optical Scatter*
  * Ratio of Cell Size:Wavelength
  * If Cell Size < Laser Wavelength (~400nm)
    * Low intensity and high inconsistency scatter

  * Measured in terms of:
     * Forward Scatter (FSC)
     * Side Scatter (SSC)

]

---

### Capture / Sorting: Size and Type

.pull-left[
*Forward Scatter (FSC)*
  * Measures along the path of the laser
  * FSC intensity proportional to diameter of cell
  * Good for distinguishing between immune cells
]

.pull-right[![:scale 85%](../img/singlecell/FlowJo_Layouts__01-Mar-2017.jpg)]


???
Diameter of cell due to light diffraction around the cell
Good for distinguishing between immune cells:
  * Monocytes and lymphocytes are two classes of white blood cells, where in general monoctyes are larger and therefore exhibit higher FSC.
  * Good for cell doublets, where two cells have been captured in the place of one, more common in well-based protocols, but can give twice the amount mRNA than a regular cell and can lead to false conclusions.

--

.pull-left[
*Side Scatter (SSC)*
  * Measures 90° to laser, along path of cells
  * Much weaker intensities than FSC
  * Refraction/reflection proportional to granularity of cell
]

.pull-right[![:scale 85%](../img/singlecell/Granulocytes_vs_Monocytes_scatter.jpg)]


???
90deg to laser -
SSC gives much weaker intensities than FSC
Cells with more intracellular structures will refract/reflect more
Can distinguish between Granulocytes and Monocytes much more easily than FSC, where Granulocytes have more cytoplasmic granules within them.

---

### Capture / Sorting: FACS

.footnote[Image from BD Biosciences]

.pull-left[![:scale 100%](../img/singlecell/CD8vsCD3.png)]

.pull-right[

*Fluorescence-Activated Cell Sorting (FACS)*
  * Cell surface markers
    * Fluorescent Markers for each cell
  * Positive and Negative
    * Whether cell activated for that CD or not.
  * Plot different CD markers against each other
    * Isolate cell populations
  * Can set gating thresholds to isolate analysis to enriched subset of cells

]

---

### Barcoding Cells

.center[![:scale 100%](../img/singlecell/scrna_pbb_barcodes_add.svg)]

* Add unique barcodes to every transcript in a cell

???
* Add them before amplification


---

### Barcoding Cells

.footnote[Place cells into sequencing plate]

.pull-left-large[![:scale 110%](../img/singlecell/scrna_pbb_barcodes_overview.svg)]

.pull-right[

* From a pool of many *many* different tissue samples / cells:

   * Cell Barcodes tell us which cell the transcript from
   * UMIs can tell us how much the transcript was amplified, by comparing it with other transcripts from the same gene with the same UMI tag.

]


---

### Sequencing Issues: Amplification

.center[![:scale 70%](../img/singlecell/amplification_errors.svg)]


* Polymerase Chain Reaction (PCR)
   * Takes a single-stranded read and duplicates it
   * Works well when enough reads are present in pool
* Low coverage
   * When reads in sequencing pool are low, many will be missed
   * Can lead to one-sided amplification


???
* Is what we see outside a true representation of what is happening inside?
* What conclusions would we draw between the expression of the red and blue transcripts?

---

### Sequencing Issues: Amp. + UMIs

.pull-left-large[
![:scale 100%](../img/singlecell/scrna_amplif_errors_umis.svg)
]

.pull-right[
* How many red transcripts in the cell?
* After PCR amplification?
* What do the little coloured tags at the start of each transcript do?


* Unique Molecular Identifiers (UMIs)
* Added to help mitigate bias from amplification.
]


---

### Sequencing Issues: Amp. + UMIs

.pull-left-large[
![:scale 100%](../img/singlecell/scrna_amplif_errors_umis.svg)
]

.pull-right[

.center[Counting Reads]

|          | # Reads |
|---------:|:-------:|
|  **Red** | 6       |
| **Blue** | 3       |

]

--

.pull-left-large[

.center[Grouping Reads by Gene and UMI]

|          | **UMIs** | **# Reads** |
|---------:|:--------:|:-----------:|
|  **Red** | Pink     | 2           |
|          | Cyan     | 4           |
| **Blue** | Pink     | 1           |
|          | Green    | 2           |

]

.pull-right[

.center[Counting de-duplicated Reads]

|          | **UMIs (Grouped)** | **# Reads** |
|---------:|:------------------:|:-----------:|
|  **Red** | {Pink, Cyan}       | 2           |
| **Blue** | {Pink, Green}      | 2           |

]

???
* UMIs qualitatively tell us how many transcripts were in the cell by *de-duplicating* amplicons.


---

### Sequencing Issues: Unique UMIs?

<!-- .footnote[[How Many mRNAs Are In a Cell](http://book.bionumbers.org/how-many-mrnas-are-in-a-cell/)] -->

.pull-left-large[![:scale 100%](../img/singlecell/scrna_amplif_errors_umis.svg)]
.pull-right[

|          | **UMIs** | **#Reads** |
|---------:|:------------------:|:-----------:|
|  **Red** | {Pink, Cyan}       | 2           |
| **Blue** | {Pink, Green}      | 2           |

* Pink appears twice in different genes.
* In what context are UMIs unique?

]

--

* Can every transcript in a cell have its own UMI?

* Number of mRNA transcripts in a cell?
  * ~ $10^5$ to $10^6$ in a mammalian cell.

* Require at minimum barcodes of length N, where $4^N = 10^5$


---

### Sequencing Issues: Unique UMIs?

.center[Barcodes of length $N$ with Edit Distance of $B$:]

.pull-left[

.center[$N = 5$ and $B = 1$]

```
AAAAA AAAAC AAAAG AAAAT AAACA ····
CCCCC CCCCA CCCCG CCCCT CCCAC ····
              ·
              ·
              ·
```

.center[$4^5 = 1024$ barcodes]

]

.pull-right[

.center[$N = 5$ and $B = 2$]

```
AAAAA AAACC AAAGG AAATT AACCA ····
CCCCC CCCAA CCCGG CCCTT CCCAA ····
              ·
              ·
              ·
```

.center[$4^{5-1} = 512$ barcodes]

]

.footnote[

Edit distances guard against **sequencing errors.**

]

???

* $B=2$ is normally used in scRNA-seq protocols.

* Must select barcodes that do *not* accidentally map to the genome.
  * Barcodes are designed to be genome-specific.
* Distance between adjacent barcodes should not be too small


---

### Sequencing Issues: Unique UMIs?

.pull-left-large[![:scale 100%](../img/singlecell/scrna_amplif_errors_umis.svg)]
.pull-right[

|          | **UMIs** | **#Reads** |
|---------:|:------------------:|:-----------:|
|  **Red** | {Pink, Cyan}       | 2           |
| **Blue** | {Pink, Green}      | 2           |

* Pink appears twice in different genes.
* In what context are UMIs unique?

]

*In what context are UMIs unique?*
  * UMIs are "random salt"
    * 'Unique enough' at the transcript level

  * We wish to count transcripts only
    * De-duplication of UMIs at transcript level
    * Good estimation of true transcript abundance

???
* UMIs can be thought of as "random salt" sprinkled into the pool.
 * If the same UMI captures two reads, each from a different transcript?
    * No problem, can still de-duplicate and get accurate transcript numbers for cell.
* As long as we can deduplicate on the gene or transcript level, the same UMI can appear across multiples.

---

### Cell Barcodes and UMIs (Recap)

For Each Cell:

1. Add Cell Barcodes to Cells
![:scale 100%](../img/singlecell/scrna_pbb_barcodes_add.svg)

---

### Cell Barcodes and UMIs (Recap)

For Each Cell:

1. Add Cell Barcodes to Cells
1. Add UMIs to Cell Barcoded Cells
![:scale 100%](../img/singlecell/scrna_umi_add.svg)


???
What is the aim of all this?
Why do we care that every mRNA molecule has a cell barcode and an UMI?

---

### QC: Overcoming Background Noise



.center[![:scale 70%](../img/singlecell/raceid_libsize.svg)]

* Num. features per cell, and library size should follow a normal curve.
* Min-Max filtering helps clip off the fat-tails of a distribution.

???
Normal curve:
 *  Num features per cell should be more or less the same
   * Cells that have very few features are either super rare transcripts or poorly sequenced cells
   * Similarly cells that have many features could be over expressed or cell doublets
QC, removing ambiguity of detection
Minimum threshold for lib size and gene detectability

---

### Normalisation: Bulk vs Single-Cell

.pull-left-large[

*Bulk RNA-seq*: High Coverage 
  
|            |  T1 | T2 | T3 |
|:-----------|----:|---:|----|
| **GeneA** | 100 | 80 | 40 |
| **GeneB** |  45 | 30 | 40 |

  * Median Gene Expression is high

]


.pull-left-large[

*scRNA-seq*: Very Low Sequencing Depth 
  
|            | C1 | C2 | C3 | C4 | C5 |
|:-----------|---:|---:|---:|---:|---:|
| **GeneA** |  0 |  0 |  2 |  0 |  1 |
| **GeneB** |  2 |  0 | 15 |  0 |  0 |

  * Median Gene Expression is zero...

]


.pull-right[

__Why is this a problem?__

]

.pull-right[

.center[
$R(s,g) = \frac{X\_{sg}}{(\prod\_{s} X\_{s})^{\frac{1}{n}}}$

$DESeq(s,g) = \frac{X\_{sg}}{Med(R\_{s})}$

]
]

--

.pull-right[
* Can't divide by zero...
]

---

### Normalisation: SCRAN method

.footnote[.small[[*Pooling across cells to normalize single-cell RNA sequencing data with many zero counts*, Lun et al., 2016](https://doi.org/10.1186/s13059-016-0947-7)]]

.pull-left[![:scale 115%](../img/singlecell/scran_pooling_left.svg)]


.pull-right[
1. Calculate the library sizes of all cells
1. Calculate the library size of a pseudo reference cell (average)
1. Separate odd sizes (red) and even sizes (blue) into two groups
1. Sort each group by library size and place on opposite sides of a "ring"

]

???
The odd / even split is just a cheap and easy way to split the group in two without having to do any fancy sampling.

Make sure the larger sizes are at 12:00 and the smaller sizes as 6:00.

---

### Normalisation: SCRAN method

.footnote[.small[[*Pooling across cells to normalize single-cell RNA sequencing data with many zero counts*, Lun et al., 2016](https://doi.org/10.1186/s13059-016-0947-7)]]

.pull-right[![:scale 115%](../img/singlecell/scran_pooling_right.svg)]

.pull-left[
1. Define overlapping pools of adjacent cells of size $k$
1. For each pool
    1. Sum the library sizes of all cells within
    1. Derive a size factor by dividing by the reference cell
1. For each cell
    1. Find which pools it belongs to 
    1. Build a linear model using these size factors
    1. Estimate the size factor of the cell on this linear model

]

???
Here, k=3

---

### Normalisation: SCRAN method

.footnote[.small[[*Pooling across cells to normalize single-cell RNA sequencing data with many zero counts*, Lun et al., 2016](https://doi.org/10.1186/s13059-016-0947-7)]]

.center[![:scale 105%](../img/singlecell/scran_pooling.svg)]

???
The advantage of this method is that by pooling cells together, you increase the likelihood that you will have non-zero median gene counts, which allows you to normalise your cells using this general linear model.

---

### Wanted vs Unwanted Variation

.pull-right[![:scale 105%](../img/singlecell/variance.svg)]

.pull-left[
*Wanted Variation*
  * Expression from the top most differentially expressed genes

*Unwanted Variation*
  * "Confounders"
  * Technical Variation
     * Batch source
     * Library Size
  * Biological Variation
     * Intrinsic cell noise
]

---

### Confounding Variation: Biological

.center[![:scale 70%](../img/singlecell/raceid_cellcycle.svg)]

.pull-left[
.center[*Transcription Bursting*]
 * Transcription not continuous, occurs in "bursts"
 * Phenomenon hidden in bulk RNA-seq
]


.pull-right[
.center[*Cell Cycle*]
 * Cells of the same type have twice the amount of mRNA at M-phase than G1-phase
]

---

### Confounding Variation: Technical

.center[![:scale 80%](../img/singlecell/raceid_technical_variation.svg)]

.pull-left[
*Amplification Bias*
* Different transcripts are amplified more than others
* Mitigated via UMIs
]

.pull-left[
*Dropout Events*
* Some genes are falsely not detected in cells
* Mitigated via better capture methods and normalisation
]

---

### Confounding Variation: Technical

.center[![:scale 80%](../img/singlecell/raceid_technical_variation.svg)]

*Library Size Variation*
* Cells have different transcription rates and capture rates
* Mitigated via normalisation

---

### Relationships Between Cells 

Consider:
 * 1,000s of Cells
 * 10,000s of Genes
 * 10k dimensional dataset, with 1k observations
 
Aim:
 * Find groupings of Cells in a subset of these Genes

Note:
* Some cells can have very similar expression in one gene, and very far different expression in all others.
* How to represent this?

???
We have a 10,000 dimensional dataset with 1,000 observations.
Are all these dimensions relevant to perform an analysis?

---

### Distance Matrix

![:scale 100%](../img/singlecell/raceid_distance.svg)

???
* This is a simplified example of an input count table represented as a scatter plot.
* Note that we have only 3 genes, and therefore 3 dimensions. 
* Note where each cell is.
* We can calculate the euclidean distance between any two points by taking the root of the sum of the squares in any of the dimensions.

---

### Relatedness of Cells: KNN

![:scale 70%](../img/singlecell/scrna_knn.svg)

* Perform *K-nearest neighbours* to connect edges to cell vertices.

---

### Dimensional Reduction

![:scale 100%](../img/singlecell/raceid_dimred.svg)

.pull-left[
*Aim:*
* Take a high-dimensional dataset and reduce it into a lower dimension that we can understand.
  * e.g. 10000-D → 2D
]
.pull-right[
*Contraint*
* Preserve the high dimensional topology in a low dimensional space.
   * e.g. if Cell A is far from Cell D yet close to Cell B in 3D, it should replicate those relationships in 2D.
]

---

### Clustering

.pull-left-large[![:scale 100%](../img/singlecell/singlecellplot3.png)]

.pull-right[
1. 2D Projection
   * Each dot is a cell
   * Clustering colours the dots, where different coloured cells belong to different clusters
   * Different clusters represent different cell types
]

---

### Clustering

.pull-left-large[![:scale 100%](../img/singlecell/singlecellplot4.png)]

.pull-right[
1. 2D Projection
1. Discrete Cell Types
  * Each cluster should represent a different type
  * Look for the most DE genes in each cluster
    * Find the marker genes → Cell Type
]

---

### Clustering

.pull-left-large[![:scale 100%](../img/singlecell/singlecellplot6.png)]

.pull-right[
1. 2D Projection
1. Discrete Cell Types
1. Relationships infer Lineage
  * Neural Stem Cells differentiate into mature cell types
  * Lineage trees are constructed by taking into account
     * Entropy of cluster
     * Proximity of cluster
]

---

<!-- ### Clustering: Heatmaps -->

<!-- * Kmedoids vs k-means vs heirarchal -->
<!--   * Outlier sensitivity -->

<!-- --- -->

### Clustering: Hard vs Soft

.pull-left[
![:scale 105%](../img/singlecell/singlecellplot3.png)

*Hard*
  * Big spaces between clusters
  * Cell types are well defined and the clustering reflects that

]


.pull-right[
![:scale 90%](../img/singlecell/10xdata.png)

*Soft*
  * Clusters bleed into one another
  * Cell types seem to intermingle with one another.

]

???
Why? Why would there be clusters so close to one another?

---

<!-- ### Clustering: Outliers -->

<!-- * What to do with outliers? -->
<!-- * Recluster them -> new cluster! -->


<!-- --- -->

### Continuous Phenotypes:

.center[![:scale 90%](../img/singlecell/raceid_contpheno.svg)]

* Cells aren't discrete, they transition
* Continuously changing over time from a less mature type to more mature type

---

### Performing Clustering

.pull-left[
Variety of different clustering methods:
 
  * K-means / K-medians
  
  * Hierarchical Clustering
  
  * Community Clustering
  
]

---

### Performing Clustering: K-means

.pull-right[![:scale 115%](../img/singlecell/kmeans.gif)]

.pull-left[
*K-means*
   1. Initialise $k$ random positions
   1. Iteration Step:
      1. Calculate distance from each cell to each $k$ position
      1. Assign each cell to it's nearest $k$
      1. Set new $k$ positions to the mean position of all cells in that group

*K-medians*
   * Same as above, but use median position instead
   * Less influenced by outliers
   
]

???
Pros / Cons : Pretty fast, but requires you to experiment with the k number a lot to find the best number of clusters and the best clustering

---

### Performing Clustering: Hierarchical

.pull-left[![:scale 75%](../img/singlecell/hierarchal1.png)]


.pull-right[
* Use the distance matrix to find the two closest points

* Merge and repeat

* Yields a dendrogram
   * Hierarchy of clusters:
  
   ![:scale 75%](../img/singlecell/hierarchal2.png)]


---


### Community Clustering: Louvain

.center[![:scale 100%](../img/singlecell/commgraph1.svg)]

Aim: Maximise internal links and minimise external links

---

### Community Clustering: Louvain

.center[![:scale 100%](../img/singlecell/commgraph2.svg)]

* Randomly pick a cell and try to place it in a neighbour's cluster
  * Accept if Internal:External increases
  * Reject and pick another

---

### Summary

.pull-left-large[![:scale 130%](../img/singlecell/rna_cells_singlerez.svg)]

.pull-right[
* Single-cell datasets are vast and sparsely populated

* Quality filtering and normalisation are required

* Feature selection and dimension reduction reduce the complexity

* Clustering denotes cell types and cell relationships

* scRNA-seq is a statistically driven field

* Many ways to analyse the data
 * Play with it!
]

---

### Further scRNA-seq Data Analysis

.center[![:scale 90%](../img/singlecell/training_single_cell.png)]

---

.center[##  ]
.center[## Thanks ]
.center[## for ]
.center[## Watching! ]